FROM mono:5.18

RUN mkdir -p /usr/src/app/source /usr/src/app/build
WORKDIR /usr/src/app/source

COPY . /usr/src/app/source
RUN nuget restore -NonInteractive && \
    xbuild /property:Configuration=Release /property:OutDir=/usr/src/app/build/

WORKDIR /usr/src/app/build

CMD ["/usr/bin/mono", "/usr/src/app/build/woofbot.exe", "startup"]
